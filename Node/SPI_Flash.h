int DO_SPI(uint8_t* cmd, uint8_t* buf, uint8_t length);
int SPI_write(uint32_t adres, uint8_t *data, uint8_t datalength);
int SPI_read(uint32_t adres, uint8_t *data, uint8_t datalength);
void SPI_INIT(void);