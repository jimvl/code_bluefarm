#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "app_uart.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "bsp.h"
#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif
#include "UART_debug.h"

/* UARTE */
#define PIN_TXD        (2)//2 testbord 13
#define PIN_RXD        (3)//3  testbord 15

void UART_tx(uint8_t CMDTX[], int lengte) //Te zenden bericht met de lengte van het bericht
{
 
      // Instellingen UART Baubrate, parity en hardware flow //
      NRF_UARTE0->CONFIG = (UART_CONFIG_HWFC_Disabled   << UART_CONFIG_HWFC_Pos) |
                           (UART_CONFIG_PARITY_Excluded << UART_CONFIG_PARITY_Pos); 
  
      NRF_UARTE0->BAUDRATE = UARTE_BAUDRATE_BAUDRATE_Baud115200 << UARTE_BAUDRATE_BAUDRATE_Pos;
  
      // TX en RX pin selecteren //
      NRF_UARTE0->PSEL.TXD = PIN_TXD;
      NRF_UARTE0->PSEL.RXD = PIN_RXD;
  
      // Enable UART//
      NRF_UARTE0->ENABLE = UARTE_ENABLE_ENABLE_Enabled << UARTE_ENABLE_ENABLE_Pos;
      NRF_UARTE0->EVENTS_ENDTX = 0;
  
      // Stel lengte TX buffer in//
      NRF_UARTE0->TXD.MAXCNT = lengte;
      NRF_UARTE0->TXD.PTR = (uint32_t)&CMDTX[0];
      //start zenden//
      NRF_UARTE0->TASKS_STARTTX = 1;
  
      // Wacht tot zend buffer leeg is//
      while (NRF_UARTE0->EVENTS_ENDTX == 0)
      {
      }

      // Stop zenden//
      NRF_UARTE0->TASKS_STOPTX = 1;
      // Wacht to DMA stop genereerd//
      while (NRF_UARTE0->EVENTS_TXSTOPPED == 0);
  
     // Disable the UARTE 
     NRF_UARTE0->ENABLE = UARTE_ENABLE_ENABLE_Disabled << UARTE_ENABLE_ENABLE_Pos;
     
}

uint8_t * UART_rx(uint8_t *rx, int lengte) //Buffer voor ontvagst berichten en de lengte van het te ontvangen bericht
{

      uint32_t i = 0;
      // Instellingen UART Baubrate, parity en hardware flow //
      NRF_UARTE0->CONFIG = (UART_CONFIG_HWFC_Disabled   << UART_CONFIG_HWFC_Pos) |
                           (UART_CONFIG_PARITY_Excluded << UART_CONFIG_PARITY_Pos); 
  
      NRF_UARTE0->BAUDRATE = UARTE_BAUDRATE_BAUDRATE_Baud115200 << UARTE_BAUDRATE_BAUDRATE_Pos;
  
      // Select TX and RX pins
      NRF_UARTE0->PSEL.TXD = PIN_TXD;
      NRF_UARTE0->PSEL.RXD = PIN_RXD;
  
      // Enable UART //
      NRF_UARTE0->ENABLE = UARTE_ENABLE_ENABLE_Enabled << UARTE_ENABLE_ENABLE_Pos;


      NRF_UARTE0->RXD.MAXCNT = lengte;
      NRF_UARTE0->RXD.PTR = (uint32_t)&rx[0];
      //Start met ontvangen//
      NRF_UARTE0->TASKS_STARTRX = 1;
      //Wacht tot DMA stop genereerd of als er ongeveer 5ms gewcht is//
      while (NRF_UARTE0->EVENTS_ENDRX == 0 && i < 1000000)
      {
          i++;
      }

      // Stop ontvangen//
      NRF_UARTE0->TASKS_STOPRX = 1;
      // Wacht tot dma stop genereerd//
      while (NRF_UARTE0->EVENTS_RXTO == 0);

      return 0;
}