/**
 * Copyright (c) 2016 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @file
 *
 * @brief fstorage example main file.
 *
 * This example showcases fstorage usage.
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "nrf.h"
#include "nrf_soc.h"
#include "nordic_common.h"
#include "boards.h"
#include "app_timer.h"
#include "app_util.h"
#include "nrf_fstorage.h"

#ifdef SOFTDEVICE_PRESENT
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_fstorage_sd.h"
#else
#include "nrf_drv_clock.h"
#include "nrf_fstorage_nvmc.h"
#endif

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"


static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);

NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    .evt_handler    = fstorage_evt_handler,
    .start_addr     = 0xfe000,
    .end_addr       = 0xfffff,
};
static uint32_t nrf5_flash_end_addr_get()
{
    uint32_t const bootloader_addr = BOOTLOADER_ADDRESS;
    uint32_t const page_sz         = NRF_FICR->CODEPAGESIZE;
    uint32_t const code_sz         = NRF_FICR->CODESIZE;

    return (bootloader_addr != 0xFFFFFFFF ?
            bootloader_addr : (code_sz * page_sz));
}

int Flash_INIT()
{

//    uint32_t laatsteadres = nrf5_flash_end_addr_get();
//    printf("laatste adres is %x\n",laatsteadres);
    nrf_fstorage_init(
        &fstorage,       /* You fstorage instance, previously defined. */
        &nrf_fstorage_sd,   /* Name of the backend. */
        NULL                /* Optional parameter, backend-dependant. */
    );
       nrf_fstorage_api_t * p_fs_api;

#ifdef SOFTDEVICE_PRESENT
    NRF_LOG_INFO("SoftDevice is present.");
    NRF_LOG_INFO("Initializing nrf_fstorage_sd implementation...");
    /* Initialize an fstorage instance using the nrf_fstorage_sd backend.
     * nrf_fstorage_sd uses the SoftDevice to write to flash. This implementation can safely be
     * used whenever there is a SoftDevice, regardless of its status (enabled/disabled). */
    p_fs_api = &nrf_fstorage_sd;
#else
    NRF_LOG_INFO("SoftDevice not present.");
    NRF_LOG_INFO("Initializing nrf_fstorage_nvmc implementation...");
    /* Initialize an fstorage instance using the nrf_fstorage_nvmc backend.
     * nrf_fstorage_nvmc uses the NVMC peripheral. This implementation can be used when the
     * SoftDevice is disabled or not present.
     *
     * Using this implementation when the SoftDevice is enabled results in a hardfault. */
    p_fs_api = &nrf_fstorage_nvmc;
#endif
}

static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result != NRF_SUCCESS)
    {
        printf("--> Event received: ERROR while executing an fstorage operation.\n");
        return;
    }

    switch (p_evt->id)
    {
        case NRF_FSTORAGE_EVT_WRITE_RESULT:
        {
            printf("--> Event received: wrote %d bytes at address 0x%x.\n",
                         p_evt->len, p_evt->addr);
        } break;

        case NRF_FSTORAGE_EVT_ERASE_RESULT:
        {
            printf("--> Event received: erased %d page from address 0x%x.\n",
                         p_evt->len, p_evt->addr);
        } break;

        default:
            break;
    }
}

void wait_for_flash_ready(nrf_fstorage_t const * p_fstorage)
{
    /* While fstorage is busy, sleep and wait for an event. */
    while (nrf_fstorage_is_busy(p_fstorage))
    {
//        power_manage();
    }
//      int i = 0;
//      while(i <= 1000000)
//      {
//        i++;
//      }
}
 
void Flash_WRITE(uint8_t *data, uint32_t adres, int lengte)
{
    ret_code_t rc;
 
//    nrf_fstorage_api_t * p_fs_api;
//
//#ifdef SOFTDEVICE_PRESENT
//    NRF_LOG_INFO("SoftDevice is present.");
//    NRF_LOG_INFO("Initializing nrf_fstorage_sd implementation...");
//    /* Initialize an fstorage instance using the nrf_fstorage_sd backend.
//     * nrf_fstorage_sd uses the SoftDevice to write to flash. This implementation can safely be
//     * used whenever there is a SoftDevice, regardless of its status (enabled/disabled). */
//    p_fs_api = &nrf_fstorage_sd;
//#else
//    NRF_LOG_INFO("SoftDevice not present.");
//    NRF_LOG_INFO("Initializing nrf_fstorage_nvmc implementation...");
//    /* Initialize an fstorage instance using the nrf_fstorage_nvmc backend.
//     * nrf_fstorage_nvmc uses the NVMC peripheral. This implementation can be used when the
//     * SoftDevice is disabled or not present.
//     *
//     * Using this implementation when the SoftDevice is enabled results in a hardfault. */
//    p_fs_api = &nrf_fstorage_nvmc;
//#endif
//
//    rc = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
//    APP_ERROR_CHECK(rc);
//
//    print_flash_info(&fstorage);
//
//    /* It is possible to set the start and end addresses of an fstorage instance at runtime.
//     * They can be set multiple times, should it be needed. The helper function below can
//     * be used to determine the last address on the last page of flash memory available to
//     * store data. */
//    (void) nrf5_flash_end_addr_get();
////    ret_code_t rc;
//    uint8_t *checkdata = data;
//    uint8_t testbuf[lengte];
//    printf("Writing \"%x\" to flash.\n", data);
//    rc = nrf_fstorage_read(&fstorage, adres, testbuf, lengte);    

    nrf_fstorage_erase(&fstorage, adres, lengte, NULL);

    rc = nrf_fstorage_write(&fstorage, adres, &data, lengte, NULL);
    APP_ERROR_CHECK(rc);
//    wait_for_flash_ready(&fstorage);
}

uint8_t * Flash_READ(uint32_t adres, int lengte)
{
    ret_code_t rc;
    uint8_t *data;
    rc = nrf_fstorage_read(&fstorage, adres, &data, lengte);
    APP_ERROR_CHECK(rc);
    wait_for_flash_ready(&fstorage);

    return data;
}

void Flash_WRITEword(uint32_t data, uint32_t adres)
{
    ret_code_t rc;
    uint32_t testdata;
//    printf("Writing \"%d\" to flash.", data);
    rc = nrf_fstorage_read(&fstorage, adres, &testdata, sizeof(testdata));
    if (data != 0)
    { 
        rc = nrf_fstorage_erase(&fstorage, adres, sizeof(data), NULL);
    }
    rc = nrf_fstorage_write(&fstorage, adres, &data, sizeof(data), NULL);
    APP_ERROR_CHECK(rc);
    wait_for_flash_ready(&fstorage);
    
}