#include "nrfx_spim.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "SPI_Flash.h"

#define NRFX_SPIM_SCK_PIN  27
#define NRFX_SPIM_MOSI_PIN 26
#define NRFX_SPIM_MISO_PIN 0
#define NRFX_SPIM_SS_PIN   1
#define NRFX_SPIM_DCX_PIN  40

#define SPI_INSTANCE  1                                           /**< SPI instance index. */
static const nrfx_spim_t spi = NRFX_SPIM_INSTANCE(SPI_INSTANCE);  /**< SPI instance. */

static volatile bool spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */


/*************Command SPI**************
***************************************/
//manufacturer data read
static uint8_t       RDID[4] = {0x9f, 0x00, 0x00, 0x00};  
static uint8_t       RDID_buf[sizeof(RDID) + 3];
static const uint8_t RDID_length = sizeof(RDID); 
//Write enable
static uint8_t       WREN[1] = {0x06};                
static uint8_t       WREN_buf[sizeof(WREN)];
static const uint8_t WREN_length = sizeof(WREN); 
//Read status register
static uint8_t       RDSR[3] = {0x05, 0x00, 0x00};                
static uint8_t       RDSR_buf[sizeof(RDSR)];
static const uint8_t RDSR_length = sizeof(RDSR); 


void spim_event_handler(nrfx_spim_evt_t const * p_event,
                       void *                  p_context)
{
    spi_xfer_done = true;
   // printf("Transfer completed.");
    if (RDID_buf[1] != 0)
    {
        printf(" Received:/n");
        printf("Dit is string:%s", RDID_buf);
    }
}

void SPI_INIT(void)
{
    nrfx_spim_config_t spi_config = NRFX_SPIM_DEFAULT_CONFIG;
    spi_config.frequency      = NRF_SPIM_FREQ_8M;
    spi_config.ss_pin         = NRFX_SPIM_SS_PIN;
    spi_config.miso_pin       = NRFX_SPIM_MISO_PIN;
    spi_config.mosi_pin       = NRFX_SPIM_MOSI_PIN;
    spi_config.sck_pin        = NRFX_SPIM_SCK_PIN;

    APP_ERROR_CHECK(nrfx_spim_init(&spi, &spi_config, spim_event_handler, NULL));
}

int SPI_write(uint32_t adres, uint8_t *data, uint8_t datalength)
{
    int status = 0;
    uint8_t PP[4 + datalength];
    uint8_t READ[4 + datalength];
    uint8_t PP_buf[sizeof(PP)];
    uint8_t READ_buf[sizeof(READ)];
    PP[0] = 0x02;
    READ[0] = 0x03;
    READ[1] = PP[1] = adres & 0xff;
    READ[2] = PP[2] = (adres >> 8) & 0xff;
    READ[3] = PP[3] = (adres >> 16) & 0xff;

    for (int i = 4; i < 4 + datalength; i++)
    {
        PP[i] = data[i - 4];
        READ[i] = 0;
        //printf("PP[%d]: %x en data[%d] : %x\n", i, PP[i], i-5, data[i - 5]);
    }

    //Check Write enable latch
    do   
    {
      //Write enable
      DO_SPI(WREN, WREN_buf, WREN_length);     

      //Read status register
      DO_SPI(RDSR, RDSR_buf, RDSR_length);               
    }while ((RDSR_buf[1] && 0b00000010) != 1);

    //Write data
    DO_SPI(PP, PP_buf, sizeof(PP));

    //Check Write in progress latch
    do
    {
        //Read status register
        DO_SPI(RDSR, RDSR_buf, RDSR_length);     
    }while ((RDSR_buf[1] && 0b00000001) == 1) ;  
    
    //Read status register
    DO_SPI(RDSR, RDSR_buf, RDSR_length);

    //Check Write enable latch = 0, QE = 0 , BP[3:0] = 0 en SWRD = 0
    if (((RDSR_buf[1] && 0b10000000) == 1) | ((RDSR_buf[1] && 0b01000000) == 1) 
      | ((RDSR_buf[1] && 0b00100000) == 1) | ((RDSR_buf[1] && 0b00000010) == 1))   
    {
        status = 3;
    }

    //Read data op zelfde adres
    DO_SPI(READ, READ_buf, sizeof(READ));

    //Check de geschreven data met de gelezen data op het adres
    for (int i = 4; i < 4 + datalength ; i++)
    {
        if (PP[i] != READ_buf[i])
        {
            status = 4;
        }
         printf("PP[%d]: %x en READ_buf[%d] : %x\n", i, PP[i], i, READ_buf[i]);
    }

    return status;
}

int SPI_read(uint32_t adres, uint8_t *getdata, uint8_t datalength)
{
    int status = 0;
    uint8_t READ[4 + datalength];
    uint8_t READ_buf[sizeof(READ)];
    READ[0] = 0x03;
    READ[1] = adres & 0xff;
    READ[2] = (adres >> 8) & 0xff;
    READ[3] = (adres >> 16) & 0xff;

    for (int i = 4; i < 4 + datalength; i++)
    {
        READ[i] = 0;
        //printf("PP[%d]: %x en data[%d] : %x\n", i, PP[i], i-5, data[i - 5]);
    }

    //Check Write in progress latch
    do
    {
        //Read status register
        DO_SPI(RDSR, RDSR_buf, RDSR_length);     
    }while ((RDSR_buf[1] && 0b00000001) == 1) ;  

    //Read data 
    DO_SPI(READ, READ_buf, sizeof(READ));

      //Laad buffer data in pointer
    for (int i = 4; i < 4 + datalength ; i++)
    {
         getdata[i-4] = READ_buf[i];
         printf("getdata[%d]: %x en READ_buf[%d] : %x\n", i-4, getdata[i-4], i, READ_buf[i]);
    }

    return status;
}
int DO_SPI(uint8_t* cmd, uint8_t* buf, uint8_t length)
{

        nrfx_spim_xfer_desc_t xfer_desc = NRFX_SPIM_XFER_TRX(cmd, length, buf, length);

        // Reset rx buffer and transfer done flag
         memset(buf, 0, length);
        spi_xfer_done = false;

        APP_ERROR_CHECK(nrfx_spim_xfer_dcx(&spi, &xfer_desc, 0, 15));

        while (!spi_xfer_done)
        {
            __WFE();
        }

        NRF_LOG_FLUSH();
 }
