#include <stdint.h>
#include <string.h>

/* HAL */
#include "boards.h"
#include "app_timer.h"
#include "app_pwm.h"

/* Core */
#include "nrf_mesh_config_core.h"
#include "nrf_mesh_gatt.h"
#include "nrf_mesh_configure.h"
#include "nrf_mesh.h"
#include "mesh_stack.h"
#include "device_state_manager.h"
#include "access_config.h"
#include "proxy.h"

/* Provisioning and configuration */
#include "mesh_provisionee.h"
#include "mesh_app_utils.h"

/* Models */
#include "generic_level_server.h"

/* Logging and RTT */
#include "log.h"
#include "rtt_input.h"

#include "apply_old_config.h"

/* Example specific includes */
#include "nrf_mesh_config_app.h"
#include "nrf.h"
#include "example_common.h"
#include "pwm_utils.h"
#include "nrf_mesh_config_examples.h"
#include "app_level.h"
#include "ble_softdevice_support.h"

///* UART */
#include <stdbool.h>
#include <stdio.h>
#include "app_config.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "nrf.h"
#include "bsp.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
//#if defined (UARTE_PRESENT)
//#include "nrf_uarte.h"
//#endif

/*UART_debug*/
#include "UART_debug.h"
#include "Blue_Farm.h"

/*Intern flash*/
#include "nrf_fstorage.h"
#include "Intern_Flash.h"

#include "flash_manager.h" // For storing custom data in flash

/*SPI program*/
#include "SPI_Flash.h"

#include "mesh_gatt.h"

extern uint8_t UUID;

/*****************************************************************************
 * Definitions
 *****************************************************************************/
#define APP_LEVEL_STEP_SIZE     (16384L)

/* Controls if the model instance should force all mesh messages to be segmented messages. */
#define APP_FORCE_SEGMENTATION  (false)
/* Controls the MIC size used by the model instance for sending the mesh messages. */
#define APP_MIC_SIZE            (NRF_MESH_TRANSMIC_SIZE_SMALL)

/* LED Settings*/

#define GROTE_LED  9
#define FREQ10K    10

#define LED1       NRF_GPIO_PIN_MAP(0,11)
#define LED2       NRF_GPIO_PIN_MAP(1,0)

#define I2B_SLAVEADDRESS  0x55
#define FANCOM_I2C_BUFFER_MAX_TX   30  // command/type + command/length + resolution/index + 25 databytes + 2 bytes crc
#define FANCOM_I2C_BUFFER_MAX_RX   30  // resolution/length + 27 databytes + 2 bytes crc

#define UART_TX_BUF_SIZE                256                                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256  


/*****************************************************************************
* Custom data in flash
 *****************************************************************************/
 #define FLASH_CUSTOM_DATA_GROUP_ELEMENT 0x1ABC // A number in the range 0x0000 - 0x7EFF (flash_manager.h)
 #define CUSTOM_DATA_FLASH_PAGE_COUNT 1

 typedef struct 
 {
   uint32_t *length;
   uint32_t data[2];
 } custom_data_format_t; // Format for the custom data

 static flash_manager_t m_custom_data_flash_manager; // flash manager instance  

 /*************Command SPI**************
***************************************/

static uint8_t       WREN[1] = {0x06};                //Write enable
static uint8_t       WREN_buf[sizeof(WREN)];
static const uint8_t WREN_length = sizeof(WREN); 

static uint8_t       RDID[4] = {0x9f, 0x00, 0x00, 0x00}; //Manufactrurer specs
static uint8_t       RDID_buf[sizeof(RDID) + 3];
static const uint8_t RDID_length = sizeof(RDID); 


/*****************************************************************************
 * Static adressen for saving diagnostic data
 *****************************************************************************/
static uint32_t adres1 = 0xAAAAAA;
static uint32_t adres2 = 0x3f200;

static void DoFancomI2C(uint8_t SLA, uint8_t Command, uint8_t Type, uint8_t Index, uint8_t Resolution, uint8_t CmdLength, uint8_t *CmdPtr, uint8_t MaxResponseLength, uint8_t *ResponsePtr);
/*****************************************************************************
 * Forward declaration of static functions
 *****************************************************************************/
static void app_level_server_set_cb(const app_level_server_t * p_server, int16_t present_level);
static void app_level_server_get_cb(const app_level_server_t * p_server, int16_t * p_present_level);
static void app_level_server_transition_cb(const app_level_server_t * p_server,
                                                uint32_t transition_time_ms, uint16_t target_level,
                                                app_transition_type_t transition_type);

static void button_event_handler(uint32_t button_number);

/*****************************************************************************
 * Static variables
 *****************************************************************************/
static bool m_device_provisioned;

/* Application level generic level server structure definition and initialization */
APP_LEVEL_SERVER_DEF(m_level_server_0,
                     APP_FORCE_SEGMENTATION,
                     APP_MIC_SIZE,
                     NULL,
                     app_level_server_set_cb,
                     app_level_server_get_cb,
                     app_level_server_transition_cb);

/* PWM hardware instance and associated variables */
/* Note: PWM cycle period determines the the max value that can be used to represent 100%
 * duty cycles, therefore present_level value scaling is required to get pwm tick value
 * between 0 and pwm_utils_contex_t:pwm_ticks_max.
 */
static APP_PWM_INSTANCE(PWM0, 1);
static app_pwm_config_t m_pwm0_config = APP_PWM_DEFAULT_CONFIG_1CH(100, FREQ10K);
static pwm_utils_contex_t m_pwm = {
                                    .p_pwm = &PWM0,
                                    .p_pwm_config = &m_pwm0_config,
                                    .channel = 0
                                  };

/* Application variable for holding instantaneous level value */
static int32_t m_pwm0_present_level;

static uint16_t adres;

/* Callback for updating the hardware state */
static void app_level_server_set_cb(const app_level_server_t * p_server, int16_t present_level)
{
    /* Resolve the server instance here if required, this example uses only 1 instance. */
    m_pwm0_present_level = present_level;
//    pwm_utils_level_set(&m_pwm, m_pwm0_present_level);
    level_application(present_level);
}

/* Callback for reading the hardware state */
static void app_level_server_get_cb(const app_level_server_t * p_server, int16_t * p_present_level)
{
    /* Resolve the server instance here if required, this example uses only 1 instance. */
    *p_present_level = m_pwm0_present_level;
    
}

/* Callback for updateing according to transition time. */
static void app_level_server_transition_cb(const app_level_server_t * p_server,
                                                uint32_t transition_time_ms, uint16_t target_level,
                                                app_transition_type_t transition_type)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Transition time: %d, Target level: %d, Transition type: %d\n",
                                       transition_time_ms, target_level, transition_type);

}

static void app_model_init(void)
{
    /* Instantiate level server on element index 0 */
    ERROR_CHECK(app_level_init(&m_level_server_0, 0));
}

/*************************************************************************************************/

static void node_reset(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "----- Node reset  -----\n");
    /* This function may return if there are ongoing flash operations. */
    mesh_stack_device_reset();
}

static void config_server_evt_cb(const config_server_evt_t * p_evt)
{
    if (p_evt->type == CONFIG_SERVER_EVT_NODE_RESET)
    {
        node_reset();
    }
}

#if NRF_MESH_LOG_ENABLE
static const char m_usage_string[] =
    "\n"
    "\t\t-------------------------------------------------------------\n"
    "\t\t RTT 1) The brightness of the LED 1 decresses in large steps.\n"
    "\t\t RTT 2) The brightness of the LED 1 incresses in large steps.\n"
    "\t\t RTT 4) Clear all the states to reset the node.\n"
    "\t\t-------------------------------------------------------------\n";
#endif

void level_application(uint32_t level) //Applicatie callback voor BlueFarm
{
 
    switch (level)
    {
        case 0x0007fff: //100% of "ON"
        {                 
            LED_blink1ms();
            break;
        }
        case 0xffffbfff: //25% of terror
        {                 
            LED_blinkterror();
            break;
        }
        case 0xffffffff: //50% open
        {           
            uint8_t sluit[7]= {13, 's', 'l', 'u', 'i', 't', 13};
            UART_tx(sluit, sizeof(sluit));
            break;
        }
        case 0x0000028e: //51% open
        {   
            uint8_t open[6]= {13, 'o', 'p', 'e', 'n', 13};
            UART_tx(open, sizeof(open));
            break;
        }
        case 0x00004ccc: //80% Lees ruwe ADC in
        {   

          uint8_t Cmd[] = 
                    { 
                          0x00,
                          0x55
                    };
                              //          Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    1, 0,  0,  0,  sizeof(Cmd), Cmd,    2, NULL); 
          break;
        } 
        case 0x00004f5b: //81% Tdirect
        {           
            uint8_t Cmd[] = 
                    { 
                          0x00,
                          0x55
                    };
                              //         Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    1, 6,  0,  0,  sizeof(Cmd), Cmd,    2, NULL); 
            break;
        }
        case 0x000051ea: //82% Percentage
        {           
            uint8_t Cmd[] = 
                    { 
                          0x00,
                          0x55
                    };
                              //         Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    1, 8,  0,  0,  sizeof(Cmd), Cmd,    2, NULL); 
            break;
        }
        case 0x0000547a: //83% Aantijd
        {           
            uint8_t Cmd[] = 
                    { 
                          0x00,
                          0x55
                    };
                              //         Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    1, 18,  0,  0,  sizeof(Cmd), Cmd,    2, NULL); 
            break;
        }
        case 0x00005709: //84% Schakelingen
        {           
            uint8_t Cmd[] = 
                    { 
                          0x00,
                          0x55
                    };
                              //         Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    1, 19,  0,  0,  sizeof(Cmd), Cmd,    2, NULL); 
            break;
        }
        case 0x00005998: //85% Type kaart
        {           
            uint8_t Cmd[] = 
                    { 
                          0x03,
                          0x00,
                          0x01
                    };
                              //         Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    27, 1,  0,  0,  sizeof(Cmd), Cmd,    3, NULL); 
            break;
        }
        case 0x00005C28: //86% versie nummer
        {           
            uint8_t Cmd[] = 
                    { 
                          0x03,
                          0x00,
                          0x02
                    };
                              //         Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    27, 2,  0,  0,  sizeof(Cmd), Cmd,    3, NULL); 
            break;
        }
        case 0x00005EB7: //87% Status
        {           
            uint8_t Cmd[] = 
                    { 
                          0x03,
                          0x00,
                          0x06
                    };
                              //         Cmd Typ Idx Res        Tsize Tbuf Rsize, Rbuf
          DoFancomI2C(I2B_SLAVEADDRESS,    27, 6,  0,  0,  sizeof(Cmd), Cmd,    3, NULL); 
            break;
        }

//        case 0x00000: //vul hier waarde in van de level
//        {           
//            //doe iets
//            break;
//        }
    }
}

static void button_event_handler(uint32_t button_number)//oude callback funtie voor knopjes
{
    /* Increase button number because the buttons on the board is marked with 1 to 4 */
    button_number++;
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Button %u pressed\n", button_number);
    switch (button_number)
    {
        /* Sending value `1` or `2` via RTT will result in LED state to change and trigger the
        STATUS message to inform client about the state change. This is a demonstration of state
        change publication due to local event. */
        case 1:
        {
            break;
        }

        case 2:
        {
            m_pwm0_present_level = (m_pwm0_present_level + APP_LEVEL_STEP_SIZE) >= INT16_MAX ?
                                   INT16_MAX : m_pwm0_present_level + APP_LEVEL_STEP_SIZE;
            break;
        }

        /* Initiate node reset */
        case 4:
        {
            /* Clear all the states to reset the node. */
            if (mesh_stack_is_device_provisioned())
            {
#if MESH_FEATURE_GATT_PROXY_ENABLED
                (void) proxy_stop();
#endif
                mesh_stack_config_clear();
                node_reset();
            }
            else
            {
                __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "The device is unprovisioned. Resetting has no effect.\n");
            }
            break;
        }

         case 10://Dit wordt aangeroepen om het PWM signaal te starten//
        {
          m_pwm0_present_level = 0;
          pwm_utils_level_set(&m_pwm, m_pwm0_present_level);
          break;
        }

        default:
            __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, m_usage_string);
            break;
    }

    if (button_number == 1 || button_number == 2)
    {
        pwm_utils_level_set(&m_pwm, m_pwm0_present_level);
        uint32_t status = app_level_current_value_publish(&m_level_server_0);
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "level: %d\n", m_pwm0_present_level);
        if ( status != NRF_SUCCESS)
        {
            __LOG(LOG_SRC_APP, LOG_LEVEL_WARN, "Unable to publish status message, status: %d\n", status);
        }
    }
}

static void app_rtt_input_handler(int key)
{
    if (key >= '1' && key <= '4')
    {
        uint32_t button_number = key - '1';
        button_event_handler(button_number);
    }
    else
    {
        __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, m_usage_string);
    }
}

static void unicast_address_print(void)
{
    dsm_local_unicast_address_t node_address;
    dsm_local_unicast_addresses_get(&node_address);
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Node Address: 0x%04x \n", node_address.address_start);
}

static void provisioning_complete_cb(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Successfully provisioned\n");

#if MESH_FEATURE_GATT_ENABLED
    /* Restores the application parameters after switching from the Provisioning
     * service to the Proxy  */
    gap_params_init();
    conn_params_init();
#endif

    unicast_address_print();
}

static void models_init_cb(void)
{
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Initializing and adding models\n");
    app_model_init();
}

static void mesh_init(void)
{
    mesh_stack_init_params_t init_params =
    {
        .core.irq_priority       = NRF_MESH_IRQ_PRIORITY_LOWEST,
        .core.lfclksrc           = DEV_BOARD_LF_CLK_CFG,
        .core.p_uuid             = NULL,
        .models.models_init_cb   = models_init_cb,
        .models.config_server_cb = config_server_evt_cb
    };

    uint32_t status = mesh_stack_init(&init_params, &m_device_provisioned);
    switch (status)
    {
        case NRF_ERROR_INVALID_DATA:
            __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Data in the persistent memory was corrupted. Device starts as unprovisioned.\n");
			__LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Reset device before start provisioning.\n");
            break;
        case NRF_SUCCESS:
            break;
        default:
            ERROR_CHECK(status);
    }
}

static void initialize(void)
{
    __LOG_INIT(LOG_SRC_APP | LOG_SRC_ACCESS | LOG_SRC_BEARER, LOG_LEVEL_INFO, LOG_CALLBACK_DEFAULT);
    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "----- BLE Mesh Dimming Server Demo -----\n");

    pwm_utils_enable(&m_pwm);

    ERROR_CHECK(app_timer_init());

    ble_stack_init();

#if MESH_FEATURE_GATT_ENABLED
    gap_params_init();
    conn_params_init();
#endif

    mesh_init();


}
static void LED_blink1ms(void) //functie knipperen LED
{
    for(int i = 0; i < 3; i++)
    {
        nrf_gpio_pin_write(GROTE_LED, 1);
        nrf_delay_ms(1);
        nrf_gpio_pin_write(GROTE_LED, 0);
        nrf_delay_ms(999);
    }

   nrf_gpio_pin_clear(GROTE_LED);
} 

static void LED_blinkterror(void) //functie knipperen LED
{
  for(int j = 0; j< 3; j++)
  {
    for(int i = 0; i < 20; i++)
    {
        nrf_gpio_pin_write(GROTE_LED, 1);
        nrf_delay_us(600);
        nrf_gpio_pin_write(GROTE_LED, 0);
        nrf_delay_ms(60);
    }
    nrf_delay_ms(1000);
   }

   nrf_gpio_pin_clear(GROTE_LED);
} 
static void start(void)
{
    rtt_input_enable(app_rtt_input_handler, RTT_INPUT_POLL_PERIOD_MS);

    if (!m_device_provisioned)
    {
        static const uint8_t static_auth_data[NRF_MESH_KEY_SIZE] = STATIC_AUTH_DATA;
        mesh_provisionee_start_params_t prov_start_params =
        {
            .p_static_data    = static_auth_data,
            .prov_sd_ble_opt_set_cb = NULL,
            .prov_complete_cb = provisioning_complete_cb,
            .prov_device_identification_start_cb = LED_blink1ms, //identificatie provisioning callback functie declaratie
            .prov_device_identification_stop_cb = NULL,
            .prov_abort_cb = NULL,
            .p_device_uri = EX_URI_DM_SERVER
        };
        ERROR_CHECK(mesh_provisionee_prov_start(&prov_start_params));
    }
    else
    {
        unicast_address_print();
    }

    mesh_app_uuid_print(nrf_mesh_configure_device_uuid_get());

    ERROR_CHECK(mesh_stack_start());

    __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, m_usage_string);

    PWM_init();
    LED_init();

    nrf_gpio_cfg_output(LED1);
    nrf_gpio_cfg_output(LED2);
    nrf_gpio_pin_write(LED1, 1);
}

static const uint16_t CRCTABLE[] = {
0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040 };

///////////////////////////////////////////////////////////////////////////////////////////////////
//
// CalcCRC16_BISYNCH
//
///////////////////////////////////////////////////////////////////////////////////////////////////
static uint16_t CalcCRC16_BISYNCH (uint8_t *ptr, uint32_t length, uint16_t initialcrc) //crc codering van Fancom
{
  uint16_t checksum = initialcrc;

  while (length--) 
    checksum = (checksum>>8) ^ CRCTABLE[(checksum & 0xff) ^ *ptr++];
  return checksum;
}

//Functie maken commando Fancom
static void DoFancomI2C(uint8_t SLA, uint8_t Command, uint8_t Type, uint8_t Index, uint8_t Resolution, uint8_t CmdLength, uint8_t *CmdPtr, uint8_t MaxResponseLength, uint8_t *ResponsePtr)
{
    static uint8_t RespBuf[FANCOM_I2C_BUFFER_MAX_TX];   // Temporary buffer to build Fancom formatted command frame
    static uint8_t CmdBuf [FANCOM_I2C_BUFFER_MAX_RX];   // Temporary buffer to receive Fancon formatted response 
    uint8_t rx[MaxResponseLength];
    uint16_t i,crc;
  
    uint8_t ResultLength;
 
    CmdBuf[0] = ((Command & 1) << 7)     | (Type & 0x7f);
    CmdBuf[1] = ((Command & 0x1e) >> 1)  | (CmdLength << 4);
    CmdBuf[2] = ((CmdLength & 0x10) >> 4)| ((Index & 0x1f) << 1) | ((Resolution & 0x03) << 6);
    memcpy(&CmdBuf[3], CmdPtr, CmdLength);
    //crc = CalcCRC16_BISYNCH(CmdBuf, CmdLength +3, 0xffff);
    CmdBuf[3+CmdLength] = crc& 0xff;
    CmdBuf[4+CmdLength] = crc;

    UART_tx(CmdBuf, (5+CmdLength));
    UART_rx(rx, MaxResponseLength);//sizeof(rx));
    UART_tx(rx, MaxResponseLength);// sizeof(rx)); 
}


 void PWM_init(void)
{
     button_event_handler(9);
}

void LED_init(void) //functie initialiseren
{
    nrf_gpio_cfg(
        GROTE_LED,
        NRF_GPIO_PIN_DIR_OUTPUT,
        NRF_GPIO_PIN_INPUT_DISCONNECT,
        NRF_GPIO_PIN_PULLDOWN,
        NRF_GPIO_PIN_S0S1,
        NRF_GPIO_PIN_NOSENSE);	
}


/*void sendDiagndata(void);

{
     typedef struct
{
    proxy_filter_t filter;
    bool connected;
    nrf_mesh_key_refresh_phase_t kr_phase;
    const nrf_mesh_beacon_info_t * p_pending_beacon_info;
    uint8_t * p_alloc_packet;
    core_tx_bearer_t bearer;
} proxy_connection_t;

proxy_connection_t * p_connection = &m_connections[p_evt->conn_index];

p_connection->connected = true;


packet_send(proxy_connection_t * p_connection);
}*/

int main(void)
 {
    int RSSI = 0;
    int status = 0;
    uint8_t diagndata[5] = {0x01, 0x02, 0x03, 0x04, 0x05};
    uint8_t diagndatareceived[5];
    initialize();
    start();
   //SPI_INIT();
    

    LED_blink1ms();  

    while (1)
    {
    
//    nrf_delay_ms(10000);
//    trigger_tx_complete();
        //Wacht op callback
      // DO_SPI(RDID, RDID_buf, RDID_length);
       //printf("teststring: %x\n", RDID_buf[1]);

//       status = SPI_write(adres1, diagndata, sizeof(diagndata));
//       printf("status: %d\n", status);

       
     //  SPI_read(adres1, diagndatareceived, sizeof(diagndatareceived));
     //  printf("opgehaalde data diagn:%x", diagndatareceived[3]);

 /*    typedef struct
{
    proxy_filter_t filter;
    bool connected;
    nrf_mesh_key_refresh_phase_t kr_phase;
    const nrf_mesh_beacon_info_t * p_pending_beacon_info;
    uint8_t * p_alloc_packet;
    core_tx_bearer_t bearer;
} proxy_connection_t;

packet_send(proxy_connection_t * p_connection);    */ 
       // nrf_delay_ms(1000);
    }   
}
